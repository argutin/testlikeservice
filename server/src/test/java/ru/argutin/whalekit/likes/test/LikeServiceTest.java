package ru.argutin.whalekit.likes.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.argutin.whalekit.likes.server.Application;
import ru.argutin.whalekit.likes.server.domain.Player;
import ru.argutin.whalekit.likes.server.service.LikeService;
import ru.argutin.whalekit.likes.server.service.PlayerService;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = Application.class)
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class LikeServiceTest {

    @Autowired
    private LikeService likeService;

    @Autowired
    private PlayerService playerService;

    private Player player;

    @BeforeEach
    void before() {
        player = playerService.create();
    }


    @DisplayName("Concurrent read-write test")
    @Test
    void readWriteTest() throws InterruptedException {
        int likesCount = 30000;

        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 4);

        final AtomicBoolean tryGetLikes = new AtomicBoolean(true);

        executorService.submit(() -> {
            while (tryGetLikes.get()) {
                likeService.getLikes(player.getId());
            }
        });

        List<Callable<Object>> collect = IntStream.range(0, likesCount)
                .mapToObj(i -> Executors.callable(() -> likeService.like(player.getId())))
                .collect(Collectors.toList());

        executorService.invokeAll(collect);
        tryGetLikes.set(false);

        assertThat(likeService.getLikes(player.getId())).isEqualTo(likesCount);
    }
}