package ru.argutin.whalekit.likes.server.domain;

import org.springframework.data.annotation.Id;

public class Player {
    @Id
    private String id;

    private long likes;

    public Player() {
    }

    public Player(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Player setId(String id) {
        this.id = id;
        return this;
    }

    public long getLikes() {
        return likes;
    }

    public Player setLikes(long likes) {
        this.likes = likes;
        return this;
    }
}
