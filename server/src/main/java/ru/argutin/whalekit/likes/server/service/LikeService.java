package ru.argutin.whalekit.likes.server.service;

import org.springframework.stereotype.Service;
import ru.argutin.whalekit.likes.server.dao.PlayerRepository;

@Service
public class LikeService {

    private final PlayerRepository playerRepository;

    public LikeService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public void like(String playerId) {
        playerRepository.incrementLikes(playerId);
    }

    public long getLikes(String playerId) {
        return playerRepository.get(playerId).getLikes();
    }
}
