package ru.argutin.whalekit.likes.server.service;

import org.springframework.stereotype.Service;
import ru.argutin.whalekit.likes.server.dao.PlayerRepository;
import ru.argutin.whalekit.likes.server.domain.Player;

@Service
public class PlayerService {

    private final PlayerRepository playerRepository;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public Player create() {
        return playerRepository.create();
    }
}
