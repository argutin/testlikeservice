package ru.argutin.whalekit.likes.server.dao;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import ru.argutin.whalekit.likes.server.domain.Player;

import java.util.UUID;

@Repository
public class PlayerRepository {

    private final MongoTemplate mongoTemplate;

    public PlayerRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public Player get(String playerId) {
        return mongoTemplate.findById(playerId, Player.class);
    }

    public void incrementLikes(String playerId) {
        mongoTemplate.updateFirst(new Query(Criteria.where("id").is(playerId)),
                new Update().inc("likes", 1),
                Player.class);
    }

    public Player create() {
        String id = UUID.randomUUID().toString();
        Player player = new Player(id);
        mongoTemplate.insert(player);
        return player;
    }
}
